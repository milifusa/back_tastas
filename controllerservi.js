var serviceModule = angular.module('OptionServiScript', ['ngSanitize']);
serviceModule.config(function ($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        'self',
        'https://www.youtube.com/**'
    ]);
});
serviceModule.directive('customOnChange', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var onChangeFunc = scope.$eval(attrs.customOnChange);
            element.bind('change', onChangeFunc);
        }
    };
});
serviceModule.controller('OptionServiController', function ($scope, $http, $rootScope, $window) {
    if (localStorage.getItem('usuario') === undefined || localStorage.getItem('usuario') === null || localStorage.getItem('usuario') === '') {
        $window.localStorage.clear();
        $window.location.href = 'auth-login.html';
    } else {
        $scope.perfiluser = JSON.parse(localStorage.getItem('perfil'));

    }
    $scope.mostrarload = false;
    $rootScope.produc_ser = "no_register";
    $scope.cerrarSesion = function () {
        $window.localStorage.clear();
        $window.location.href = 'index.html';
    };


    var data = {
        "evento": 'leerEspecialidad'
    }
    $http.post("https://mishulojan.com/www.option2.com/option2.php", data).then(function (response) {
        $rootScope.Especialidad = response.data;
    });


    $scope.intitucionact = JSON.parse(localStorage.getItem('institucionactivo'));
    $scope.mostrarload = true;
    var datalogin = {
        "evento": 'GiroNeg',
    }
    $http.post("https://mishulojan.com/www.option2.com/option2.php", datalogin).then(function (response) {
        $scope.mostrarload = false;
        if (response.data === '"no_register"') {
            $rootScope.giro_n = "no_register";
        } else {
            $rootScope.giro_n = response.data;
        }
    }, function (data) {
        $scope.mostrarload = false;
    });


    var datalogin = {
        "evento": 'SelectProdByInsti',
        "Id_institucion": $scope.intitucionact.Id_institucion
    }
    $http.post("https://mishulojan.com/www.option2.com/option2.php", datalogin).then(function (response) {
        console.log(response)
        $scope.mostrarload = false;
        if (response.data === '"no_register"') {
            $rootScope.produc_ser = "no_register";
        } else {
            $rootScope.produc_ser = response.data;
        }
    }, function (data) {
        $scope.mostrarload = false;
    });



    $scope.SaveProd = function (nombre_comercial, nombre_generico, especialid, sexo, edad_1, edad_2, descripcion, giro_n) {
        $scope.mostrarload = true;
        var datalogin = {
            "evento": 'addProdServ',
            "Nombre_comercial": nombre_comercial,
            "Nombre_generico": nombre_generico,
            "Sexo": sexo,
            "Edad_1": edad_1,
            "Edad_2": edad_2,
            "Descripcion": descripcion,
            "Id_institucion": $scope.intitucionact.Id_institucion,
            "Giro_neg": giro_n
        }
        $http.post("https://mishulojan.com/www.option2.com/option2.php", datalogin).then(function (response) {
            console.log(response)
                // var dataCom = {
                //     "evento": 'ultimoProd'
                // }
                // $http.post("https://mishulojan.com/www.option2.com/option2.php", dataCom).then(function (response) {
                //     especialid.forEach(element => {
                //         var dataCom = {
                //             "evento": 'addTemProdEspe',
                //             "Id_especialidades": element,
                //             "Id_prod_serv": response.data.Id_prod_serv
                //         }
                //         $http.post("https://mishulojan.com/www.option2.com/option2.php", dataCom).then(function (response) {
                //             $scope.mostrarload = false;
                //             $window.location.href = 'productos_servicios.html';
                //         });
                //     });
                // }, function (data) {
                //     $scope.mostrarload = false;
                // });
        });
    }

    

    


});