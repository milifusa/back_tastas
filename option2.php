<?php
    require 'conexion.php';
    // TOMAMOS NUESTRO JSON RECIBIDO DESDE LA PETICION DE ANGULAR JS Y LO LEEMOS
    $JSON       = file_get_contents("php://input");
    $request    = json_decode($JSON);
    $key        = $request->evento;


    switch($key)
    {
        case "login_Usuario":
            $usuario    = $request->usuario; 
            $contrasena = $request->contrasena; 
            consultarLogin($usuario,$contrasena);
            break;
        case 'consultarUsersEmail':
            $Email   = $request->Email; 
            consultarUsersEmail($Email);
            break;
        case 'searchContenido':
            $texto_buscado = $request->texto_buscado;
            searchContenido($texto_buscado);
            break;
        case 'VistaPrincipal':
            $sexo            = $request->sexo; 
            $edad1           = $request->edad1; 
            $edad2           = $request->edad2; 
            $infantil       = $request->infantil;
            VistaPrincipal($sexo,$edad1,$edad2,$infantil);
            break;
        case 'leerCiudad':
            leerCiudad();
            break;
        case 'guardarUsuario':
            $Nombres           = $request->Nombres; 
            $Email             = $request->Email; 
            $Celular           = $request->Celular; 
            $Telefono          = $request->Telefono; 
            $Contrasena        = $request->Contrasena; 
            $Tipo_usuario      = $request->Tipo_usuario;
            $Fecha_Nacimiento  = $request->Fecha_Nacimiento; 
            $Idciudad          = $request->Idciudad; 
            $Genero            = $request->Genero;
            $Child             = $request->Child;
            $Foto              = $request->Foto;
            guardarUsuario($Nombres,$Email,$Celular,$Telefono,$Contrasena,$Tipo_usuario,$Fecha_Nacimiento,$Idciudad,$Genero, $Child,$Foto);
            break;
        case 'leerEspecialidad':
            leerEspecialidad();
            break;
        case 'guardarDoc':
            $licencia_dor         = $request->licencia_dor; 
            $consultorio_dir      = $request->consultorio_dir; 
            $id_usuario           = $request->id_usuario; 
            $Intro_corta          = $request->Intro_corta; 
            $Intro_larga          = $request->Intro_larga; 
            $Facebook             = $request->Facebook; 
            $Instagram            = $request->Instagram; 
            $Twitter              = $request->Twitter; 
            $Titulo               = $request->Titulo;
            $Educacion            = $request->Educacion; 
            $Experiencia          = $request->Experiencia; 
            $Linkedin             = $request->Linkedin;
            guardarDoc($licencia_dor,$consultorio_dir,$id_usuario,$Intro_corta,$Intro_larga,$Facebook,$Instagram,$Twitter,$Titulo,$Educacion,$Experiencia,$Linkedin);
            break;
        case 'UpdateDoc':
            $licencia_dor         = $request->licencia_dor; 
            $consultorio_dir      = $request->consultorio_dir; 
            $id_usuario           = $request->id_usuario; 
            $Intro_corta          = $request->Intro_corta; 
            $Intro_larga          = $request->Intro_larga; 
            $Facebook             = $request->Facebook; 
            $Instagram            = $request->Instagram; 
            $Twitter              = $request->Twitter; 
            $Titulo               = $request->Titulo;
            $Educacion            = $request->Educacion; 
            $Experiencia          = $request->Experiencia; 
            $Linkedin             = $request->Linkedin;
            UpdateDoc($licencia_dor,$consultorio_dir,$Intro_corta,$Intro_larga,$Facebook,$Instagram,$Twitter,$Titulo,$Educacion,$Experiencia,$Linkedin,$id_usuario);
            break;
        case 'ultimoDoc':
            ultimoDoc();
            break;
        case 'guardarDoc_espe':
            $Id_especialidad    = $request->Id_especialidad; 
            $id_doctor          = $request->id_doctor; 
            guardarDoc_espe($Id_especialidad,$id_doctor);
            break;
        case 'guardarDoc_inte':
            $Id_especialidad    = $request->Id_especialidad; 
            $id_doctor          = $request->id_doctor; 
            guardarDoc_inte($Id_especialidad,$id_doctor);
            break;
        case 'eliminarDoc_espe':
            $Id_especialidad    = $request->Id_especialidad; 
            $id_doctor          = $request->id_doctor; 
            eliminarDoc_espe($Id_especialidad,$id_doctor);
            break;
        case 'SelecAreas1':
            $areas1         = $request->areas1; 
            $areas2         = $request->areas2; 
            $varmomentanea  = $request->varmomentanea; 
            $edad1          = $request->edad1; 
            $edad2          = $request->edad2;
            $sexo           = $request->sexo;
            $infantil       = $request->infantil;
            SelecAreas1($areas1,$areas2,$varmomentanea,$edad1,$edad2,$sexo,$infantil);
            break;
        case 'dataDoc':
            $Id_doctor      = $request->Id_doctor;
            dataDoc($Id_doctor);
            break;
        case 'SelecAreasDocs':
            $id_doctor      = $request->id_doctor;
            SelecAreasDocs($id_doctor);
            break;
        case 'SelecInterDocs':
            $id_doctor      = $request->id_doctor;
            SelecInterDocs($id_doctor);
            break;
        case 'EnferByDoc':
            $id_doctor      = $request->id_doctor;
            EnferByDoc($id_doctor);
            break;
        case 'guardarArtPers':
            $Id_enfermedad   = $request->Id_enfermedad; 
            $usuario         = $request->usuario; 
            guardarArtPers($Id_enfermedad,$usuario);
            break;
        case 'EnferByUseraved':
            $usuario         = $request->usuario; 
            EnferByUseraved($usuario);
            break;
        case 'guardarInfoArt':
            $Nombres         = $request->Nombres; 
            $Telefono        = $request->Telefono; 
            $Email           = $request->Email; 
            $Pregunta        = $request->Pregunta; 
            $Fecha_inicio    = $request->Fecha_inicio;
            $Id_enfermedad   = $request->Id_enfermedad;
            $Id_doc          = $request->Id_doc; 
            $Revisado        = $request->Revisado; 
            guardarInfoArt($Nombres,$Telefono,$Email,$Pregunta,$Fecha_inicio,$Id_enfermedad,$Id_doc,$Revisado);
            break;
        case 'updateLike':
            $likes              = $request->likes; 
            $Id_enfermedad      = $request->Id_enfermedad; 
            updateLike($likes, $Id_enfermedad);
            break;
        case 'guardarLikesU':
            $usuario              = $request->usuario; 
            $Id_enfermedad      = $request->Id_enfermedad; 
            guardarLikesU($Id_enfermedad,$usuario);
            break;
        case 'deleteLikesU':
            $usuario              = $request->usuario; 
            $Id_enfermedad      = $request->Id_enfermedad; 
            deleteLikesU($Id_enfermedad,$usuario);
            break;
        case 'EnferByid':
            $Id_enfermedad      = $request->Id_enfermedad; 
            EnferByid($Id_enfermedad);
            break;
        case 'DocById':
            $id_usuario      = $request->id_usuario; 
            DocById($id_usuario);
            break;
        case 'EnfermEDICOSS':
            $sql      = $request->sql; 
            EnfermEDICOSS($sql);
            break;
        case 'GuardarFeed':
            $Nombre_enfermedad      = $request->Nombre_enfermedad; 
            $Areas                  = $request->Areas; 
            $video_url              = $request->video_url; 
            $video_url2             = $request->video_url2; 
            $texto_corto            = $request->texto_corto;
            $texto_url              = $request->texto_url;
            $Sexo                   = $request->Sexo; 
            $Edad_1                 = $request->Edad_1; 
            $Edad_2                 = $request->Edad_2; 
            $sintomas               = $request->sintomas; 
            $causas                 = $request->causas; 
            $tratamiento            = $request->tratamiento; 
            $diagnostico            = $request->diagnostico; 
            $Nombre_video           = $request->Nombre_video; 
            $Id_doctor              = $request->Id_doctor; 
            $relacionadas           = $request->relacionadas;
            GuardarFeed($Nombre_enfermedad,$Areas,$video_url, $video_url2,$texto_corto,$texto_url,$Sexo, $Edad_1, $Edad_2,$sintomas, $causas, $tratamiento, $diagnostico, $Nombre_video, $Id_doctor,$relacionadas);
            break;
        case 'EditFeed':
            $Nombre_enfermedad      = $request->Nombre_enfermedad; 
            $Areas                  = $request->Areas; 
            $video_url              = $request->video_url; 
            $video_url2             = $request->video_url2; 
            $texto_corto            = $request->texto_corto;
            $texto_url              = $request->texto_url;
            $Sexo                   = $request->Sexo; 
            $Edad_1                 = $request->Edad_1; 
            $Edad_2                 = $request->Edad_2; 
            $sintomas               = $request->sintomas; 
            $causas                 = $request->causas; 
            $tratamiento            = $request->tratamiento; 
            $diagnostico            = $request->diagnostico; 
            $Nombre_video           = $request->Nombre_video; 
            $Id_enfermedad          = $request->Id_enfermedad; 
            $relacionadas           = $request->relacionadas;
            EditFeed($Nombre_enfermedad,$Areas,$video_url,$video_url2,$texto_corto,$texto_url,$Sexo,$Edad_1, $Edad_2,$sintomas, $causas, $tratamiento,$diagnostico, $Nombre_video,$relacionadas,$Id_enfermedad);
            break;
        case 'updateUsuario':
            $Nombres           = $request->Nombres; 
            $Email             = $request->Email; 
            $Celular           = $request->Celular; 
            $Telefono          = $request->Telefono; 
            $Fecha_Nacimiento  = $request->Fecha_Nacimiento; 
            $Idciudad          = $request->Idciudad; 
            $Genero            = $request->Genero;
            $Foto              = $request->Foto;
            updateUsuario($Nombres,$Email,$Celular,$Telefono,$Fecha_Nacimiento,$Idciudad,$Genero,$Foto);
            break;
        case 'consultarId_enfer':
            $Id_enfermedad     = $request->Id_enfermedad;
            consultarId_enfer($Id_enfermedad);
            break;
        case 'addCasos':
            $Sexo                   = $request->Sexo; 
            $Edad_1                 = $request->Edad_1; 
            $Edad_2                 = $request->Edad_2; 
            $Descripcion            = $request->Descripcion; 
            $Sintomas               = $request->Sintomas; 
            $url_video              = $request->url_video; 
            $Nombres_posibles       = $request->Nombres_posibles;
            $causas                 = $request->causas;
            $resultados_exame       = $request->resultados_exame; 
            $doctores_visitados     = $request->doctores_visitados; 
            $tratamientos           = $request->tratamientos; 
            $id_doc                 = $request->id_doc; 
            addCasos($Sexo,$Edad_1, $Edad_2,$Descripcion,$Sintomas,$url_video,$Nombres_posibles, $causas, $resultados_exame, $doctores_visitados, $tratamientos,$id_doc);
            break;
        case 'UpdateCasos':
            $Sexo                   = $request->Sexo; 
            $Edad_1                 = $request->Edad_1; 
            $Edad_2                 = $request->Edad_2; 
            $Descripcion            = $request->Descripcion; 
            $Sintomas               = $request->Sintomas; 
            $url_video              = $request->url_video; 
            $Nombres_posibles       = $request->Nombres_posibles;
            $causas                 = $request->causas;
            $resultados_exame       = $request->resultados_exame; 
            $doctores_visitados     = $request->doctores_visitados; 
            $tratamientos           = $request->tratamientos; 
            $Id_casos                 = $request->Id_casos; 
            UpdateCasos($Sexo,$Edad_1, $Edad_2,$Descripcion,$Sintomas,$url_video,$Nombres_posibles, $causas, $resultados_exame, $doctores_visitados, $tratamientos,$Id_casos);
            break;
        case 'cuentaTotCasosDoc':
            $id_doc                 = $request->id_doc; 
            cuentaTotCasosDoc($id_doc);
            break;
        case 'cuentaTotCasos':
            cuentaTotCasos();
            break;
        case 'CasosExep':
            CasosExep();
            break;
        case 'CasosExepDoc':
            $id_doc                 = $request->id_doc; 
            CasosExepDoc($id_doc);
            break;
        case 'ComentCasos':
            $Id_caso                 = $request->Id_caso; 
            ComentCasos($Id_caso);
            break;
        case 'addComent':
            $Id_caso             = $request->Id_caso; 
            $Id_usuario          = $request->Id_usuario; 
            $Comentario          = $request->Comentario; 
            $Nombres_coment      = $request->Nombres_coment; 
            $Foto                = $request->Foto; 
            $id_doc              = $request->id_doc; 
            $fecha_coment        = $request->fecha_coment;
            addComent($Id_caso,$Id_usuario, $Comentario,$Nombres_coment,$Foto,$id_doc,$fecha_coment);
            break;
        case 'GiroNeg':
            GiroNeg();
            break;
        case 'guardarInstitucion':
            $usuario            = $request->usuario; 
            $ruc                = $request->ruc; 
            $Nombrecomercial    = $request->Nombrecomercial; 
            $Razonsocial        = $request->Razonsocial; 
            $Facebook           = $request->Facebook; 
            $Twitter            = $request->Twitter; 
            $Instagram          = $request->Instagram;
            $Linkedin           = $request->Linkedin; 
            $Pagina_web         = $request->Pagina_web;
            guardarInstitucion($usuario,$ruc,$Nombrecomercial,$Razonsocial,$Facebook,$Twitter,$Instagram,$Linkedin,$Pagina_web);
            break;
        case 'ultimoInsti':
            ultimoInsti();
            break;
        case 'guardarInsti_giro':
            $Id_giro            = $request->Id_giro; 
            $Id_institucion     = $request->Id_institucion; 
            guardarInsti_giro($Id_giro,$Id_institucion);
            break;
        case 'InstById':
            $usuario            = $request->usuario; 
            InstById($usuario);
            break;
        case 'SelectInstiGiro':
            $Id_institucion     = $request->Id_institucion; 
            SelectInstiGiro($Id_institucion);
            break;
        case 'addProdServ':
            $Nombre_comercial           = $request->Nombre_comercial; 
            $Nombre_generico            = $request->Nombre_generico; 
            $Sexo                       = $request->Sexo; 
            $Edad_1                     = $request->Edad_1; 
            $Edad_2                     = $request->Edad_2; 
            $Descripcion                = $request->Descripcion; 
            $Id_institucion             = $request->Id_institucion;
            $Giro_neg                   = $request->Giro_neg;
            addProdServ($Nombre_comercial,$Nombre_generico, $Sexo,$Edad_1,$Edad_2,$Descripcion,$Id_institucion,$Giro_neg);
            break;
        case 'addTemProdEspe':
            $Id_prod_serv               = $request->Id_prod_serv; 
            $Id_especialidades          = $request->Id_especialidades;
            addTemProdEspe($Id_prod_serv,$Id_especialidades);
            break;
        case 'SelecTemProdEspe ':
            $Id_prod_serv               = $request->Id_prod_serv; 
            SelecTemProdEspe($Id_prod_serv);
            break;
        case 'SelectProdByInsti':
            $Id_institucion             = $request->Id_institucion;
            SelectProdByInsti($Id_institucion);
            break;
        case 'SelectProdByGiro':
            $Giro_neg                   = $request->Giro_neg;
            SelectProdByGiro($Giro_neg);
            break;
        case 'ultimoProd':
            ultimoProd();
            break;
    }

    function consultarLogin($usuario,$contrasena){
        $sql ="SELECT * FROM usuario WHERE Email = '$usuario' and Contrasena= '$contrasena' "; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            $usuario = $stmt->fetchObject();
            $db = null;
            echo  json_encode($usuario);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    }

    function UpdateDoc($licencia_dor,$consultorio_dir,$Intro_corta,$Intro_larga,$Facebook,$Instagram,$Twitter,$Titulo,$Educacion,$Experiencia,$Linkedin,$id_usuario){
        $sql ="UPDATE Doctor SET licencia_dor='$licencia_dor',consultorio_dir='$consultorio_dir',Intro_corta='$Intro_corta',Intro_larga='$Intro_larga',Facebook='$Facebook',Instagram='$Instagram',Twitter='$Twitter',Titulo='$Titulo',Educacion='$Educacion',Experiencia='$Experiencia',Linkedin='$Linkedin' WHERE id_usuario='$id_usuario'";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("Usuario correctamente");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };    


    function consultarUsersEmail($Email){
        $sql ="SELECT * FROM usuario WHERE Email = '$Email'"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    }
    
    function consultarUsers($Cedula_i){
        $sql ="SELECT * FROM usuario WHERE Cedula_i = '$Cedula_i'"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            $usuario = $stmt->fetchObject();
            $db = null;
            echo  json_encode($usuario);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    }
    function consultarId_enfer($Id_enfermedad){
        $sql ="SELECT * FROM Enfermedad WHERE Id_enfermedad = '$Id_enfermedad'"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            $usuario = $stmt->fetchObject();
            $db = null;
            echo  json_encode($usuario);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    }

    function searchContenido($texto_buscado){
        $sql ="SELECT Enfermedad.Id_enfermedad, Enfermedad.Valor, Enfermedad.Nombre_enfermedad,Enfermedad.video_url, Enfermedad.texto_url, Enfermedad.texto_url, 
        Enfermedad.Likes, Enfermedad.video_url2, Doctor.Id_doctor,usuario.Nombres, usuario.Email, usuario.Celular, usuario.Telefono, usuario.Foto ,
        Enfermedad.texto_corto, Enfermedad.Nombre_video FROM Enfermedad, Doctor,usuario WHERE Doctor.Id_doctor=Enfermedad.Id_doctor AND
         usuario.Email=Doctor.id_usuario AND MATCH( Nombre_enfermedad,Areas,terminos,organos_involucrados, causas,sintomas,diagnostico,tratamiento,
         Sexo,Enfermedades_relacionadas,Nombre_video) AGAINST ('$texto_buscado') LIMIT 20"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };

    function VistaPrincipal($sexo,$edad1,$edad2,$infantil){

        if($infantil==="no"){
            $sql ="SELECT * FROM (SELECT enf.Id_enfermedad, enf.Valor,enf.Nombre_enfermedad, enf.video_url, enf.texto_url, enf.Likes, doc.Id_doctor,
            us.Nombres, us.Email, us.Celular, us.Telefono, us.Foto,enf.texto_corto, enf.Nombre_video FROM Enfermedad enf, Doctor doc,usuario us WHERE 
            enf.Id_doctor=doc.Id_doctor AND us.Email=doc.id_usuario AND (enf.Sexo like '%$sexo%' or enf.Sexo ='$sexo' ) and enf.Edad_1<='$edad1' 
            and enf.Edad_2>='$edad2'  and enf.Areas  not LIKE'%infantil%' and enf.Areas  not LIKE'%pediatria%' order by rand() LIMIT 8) AS 
            `some_table_name_lol_this_is_an_alias` order by Valor ASC";
             
        }else{
            $sql ="SELECT * FROM (SELECT enf.Id_enfermedad, enf.Valor,enf.Nombre_enfermedad, enf.video_url, enf.texto_url, enf.Likes, doc.Id_doctor,
            us.Nombres, us.Email, us.Celular, us.Telefono, us.Foto,enf.texto_corto, enf.Nombre_video FROM Enfermedad enf, Doctor doc,usuario us WHERE 
            enf.Id_doctor=doc.Id_doctor AND us.Email=doc.id_usuario AND (enf.Sexo like '%$sexo%' or enf.Sexo ='$sexo' ) and enf.Edad_1<='$edad1' 
            and enf.Edad_2>='$edad2' order by rand() LIMIT 8) AS `some_table_name_lol_this_is_an_alias` order by Valor ASC";

        }
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };

    function leerCiudad(){
        $sql ="SELECT * FROM Ciudad "; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };

    function leerEspecialidad(){
        $sql ="SELECT * FROM Especialidades order by Nombre_especialidad "; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };

    function guardarUsuario($Nombres,$Email,$Celular,$Telefono,$Contrasena,$Tipo_usuario,$Fecha_Nacimiento,$Idciudad,$Genero,$Child,$Foto){
        $sql ="INSERT INTO usuario (Nombres,Email,Celular,Telefono,Contrasena,Tipo_usuario,Fecha_Nacimiento,Idciudad,Genero,Hijos,Foto) VALUES ('$Nombres','$Email','$Celular','$Telefono','$Contrasena','$Tipo_usuario','$Fecha_Nacimiento','$Idciudad','$Genero','$Child','$Foto')";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("Usuario correctamente");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };

    function updateUsuario($Nombres,$Email,$Celular,$Telefono,$Fecha_Nacimiento,$Idciudad,$Genero,$Foto){
        $sql ="UPDATE usuario SET Nombres='$Nombres', Celular='$Celular',Telefono='$Telefono', Fecha_Nacimiento='$Fecha_Nacimiento',Idciudad='$Idciudad',Genero='$Genero',Foto='$Foto', Email='$Email'    WHERE Email='$Email' ";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("Usuario update");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };


    function guardarDoc($licencia_dor,$consultorio_dir,$id_usuario,$Intro_corta,$Intro_larga,$Facebook,$Instagram,$Twitter,$Titulo,$Educacion,$Experiencia,$Linkedin){
        $sql ="INSERT INTO Doctor (licencia_dor,consultorio_dir,id_usuario,Intro_corta,Intro_larga,Facebook,Instagram,Twitter,Titulo,Educacion,Experiencia,Linkedin) VALUES ('$licencia_dor','$consultorio_dir','$id_usuario','$Intro_corta','$Intro_larga','$Facebook','$Instagram','$Twitter','$Titulo','$Educacion','$Experiencia','$Linkedin')";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("Doctor correctamente");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };

    function guardarInstitucion($usuario,$ruc,$Nombrecomercial,$Razonsocial,$Facebook,$Twitter,$Instagram,$Linkedin,$Pagina_web){
        $sql ="INSERT INTO Instituciones (usuario,ruc,Nombrecomercial, Razonsocial,Facebook,Twitter,Instagram,Linkedin,Pagina_web) VALUES('$usuario','$ruc','$Nombrecomercial','$Razonsocial','$Facebook','$Twitter','$Instagram','$Linkedin','$Pagina_web')";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("Intitucion correctamente");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    }

    function ultimoDoc(){
        $sql ="SELECT * FROM Doctor order by Id_doctor desc limit 1"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            $contacto = $stmt->fetchObject();
            $db = null;
            echo  json_encode($contacto);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };
    function ultimoInsti(){
        $sql ="SELECT * FROM Instituciones order by Id_institucion desc limit 1"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            $contacto = $stmt->fetchObject();
            $db = null;
            echo  json_encode($contacto);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };

    function guardarDoc_espe($Id_especialidad,$id_doctor){
        $sql ="INSERT INTO doc_especialidades_temp (Id_especialidad,id_doctor) VALUES ('$Id_especialidad','$id_doctor')";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("Doctor correctamente");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };
    function guardarInsti_giro($Id_giro,$Id_institucion){
        $sql ="INSERT INTO Inst_giro_temp (Id_giro,Id_institucion) VALUES ('$Id_giro','$Id_institucion')";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("giro_temp correctamente");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };

    function guardarDoc_inte($Id_especialidad,$id_doctor){
        $sql ="INSERT INTO doc_intereses_temp (Id_especialidad,id_doctor) VALUES ('$Id_especialidad','$id_doctor')";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("Doctor correctamente");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };
    

    function eliminarDoc_espe ($Id_especialidad,$id_doctor){
        $sql ="DELETE FROM doc_especialidades_temp WHERE Id_especialidad='$Id_especialidad' and id_doctor='$id_doctor'";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("Area eliminada");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };

    function SelecAreas1($areas1,$areas2,$varmomentanea,$edad1,$edad2,$sexo,$infantil){

        if($infantil==="no"){
            $sql ="SELECT Enfermedad.Id_enfermedad,Enfermedad.Nombre_enfermedad,Enfermedad.video_url, Enfermedad.texto_url, Enfermedad.texto_url, 
        Enfermedad.Likes, Enfermedad.video_url2, Doctor.Id_doctor,usuario.Nombres, usuario.Email, usuario.Celular, usuario.Telefono, usuario.Foto ,
        Enfermedad.texto_corto, Enfermedad.Nombre_video FROM Enfermedad, Doctor,usuario WHERE Doctor.Id_doctor=Enfermedad.Id_doctor AND usuario.Email=Doctor.id_usuario AND 
        (Enfermedad.Areas LIKE'%$areas1%' OR Enfermedad.Areas LIKE'%$areas2%') and Enfermedad.Sexo LIKE '%$sexo%' and Enfermedad.Areas  not LIKE'%infantil%' and Enfermedad.Areas  not LIKE'%pediatria%' 
        ORDER BY CASE WHEN Enfermedad.Nombre_enfermedad LIKE'%$varmomentanea%' THEN 1 WHEN Enfermedad.Areas LIKE'%$areas1%' THEN 2 WHEN Enfermedad.Areas LIKE'%$areas2%' THEN 3 WHEN Enfermedad.Edad_1 >'$edad1' THEN 4 WHEN Enfermedad.Edad_2 <'$edad2' THEN 5 else 6 end LIMIT 15"; 
        }else{
            $sql ="SELECT Enfermedad.Id_enfermedad,Enfermedad.Nombre_enfermedad,Enfermedad.video_url, Enfermedad.texto_url, Enfermedad.texto_url, 
            Enfermedad.Likes, Enfermedad.video_url2, Doctor.Id_doctor,usuario.Nombres, usuario.Email, usuario.Celular, usuario.Telefono, usuario.Foto ,
            Enfermedad.texto_corto , Enfermedad.Nombre_video FROM Enfermedad, Doctor,usuario WHERE Doctor.Id_doctor=Enfermedad.Id_doctor AND usuario.Email=Doctor.id_usuario AND 
            (Enfermedad.Areas LIKE'%$areas1%' OR Enfermedad.Areas LIKE'%$areas2%') and Enfermedad.Sexo LIKE '%$sexo%' ORDER BY CASE WHEN Enfermedad.Nombre_enfermedad LIKE'%$varmomentanea%' THEN 1 WHEN Enfermedad.Areas LIKE'%$areas1%' THEN 2 WHEN Enfermedad.Areas LIKE'%$areas2%' THEN 3 WHEN Enfermedad.Edad_1 >'$edad1' THEN 4 WHEN Enfermedad.Edad_2 <'$edad2' THEN 5 else 6 end LIMIT 15"; 
        }
        
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };


    function dataDoc($Id_doctor){
        $sql ="SELECT Doctor.Id_doctor, Doctor.licencia_dor, Doctor.consultorio_dir, Doctor.Intro_corta, Doctor.Intro_larga, Doctor.Facebook, Doctor.Instagram, Doctor.Twitter, Doctor.Titulo,  Doctor.Educacion,  Doctor.Experiencia, usuario.Nombres,usuario.Email,usuario.Celular,usuario.Telefono, usuario.Fecha_Nacimiento,usuario.Foto, Ciudad.Ciudad from Doctor, usuario,Ciudad WHERE usuario.Email=Doctor.id_usuario AND Ciudad.Id_ciudad=usuario.Idciudad AND Doctor.Id_doctor='$Id_doctor'"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            $contacto = $stmt->fetchObject();
            $db = null;
            echo  json_encode($contacto);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };
    function SelecAreasDocs($id_doctor){
        $sql =" SELECT Especialidades.Nombre_especialidad, Especialidades.Id_especialidad FROM Especialidades, Doctor,doc_especialidades_temp WHERE Doctor.Id_doctor=doc_especialidades_temp.id_doctor AND Especialidades.Id_especialidad=doc_especialidades_temp.Id_especialidad AND doc_especialidades_temp.id_doctor='$id_doctor'"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };
    function SelecInterDocs($id_doctor){
        $sql =" SELECT Especialidades.Nombre_especialidad, Especialidades.Id_especialidad FROM Especialidades, Doctor,doc_intereses_temp WHERE Doctor.Id_doctor=doc_intereses_temp.id_doctor AND Especialidades.Id_especialidad=doc_intereses_temp.Id_especialidad AND doc_intereses_temp.id_doctor='$id_doctor'"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };
    function SelectInstiGiro($Id_institucion){
        $sql ="SELECT  Giro_negocio.Nombre, Giro_negocio.Id_giro FROM Giro_negocio, Instituciones ,Inst_giro_temp WHERE  Instituciones.Id_institucion=Inst_giro_temp.Id_institucion AND   Giro_negocio.Id_giro= Inst_giro_temp.Id_giro AND Inst_giro_temp.Id_institucion='$Id_institucion'"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };

    

    

    function EnferByDoc($id_doctor){
        $sql ="SELECT Enfermedad.Id_enfermedad,Enfermedad.Nombre_enfermedad,Enfermedad.video_url, Enfermedad.texto_url, Enfermedad.texto_url, 
        Enfermedad.Likes, Enfermedad.video_url2, Doctor.Id_doctor,usuario.Nombres, usuario.Email, usuario.Celular, usuario.Telefono, usuario.Foto ,
        Enfermedad.texto_corto, Enfermedad.Nombre_video FROM Enfermedad, Doctor,usuario WHERE Doctor.Id_doctor=Enfermedad.Id_doctor AND usuario.Email=Doctor.id_usuario AND Enfermedad.Id_doctor='$id_doctor' ORDER BY Enfermedad.Id_enfermedad DESC  LIMIT 15"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };

    function guardarArtPers($Id_enfermedad,$usuario){
        $sql ="INSERT INTO GuardadosAart (Id_enfermedad,usuario) VALUES ('$Id_enfermedad','$usuario')";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("add_ok");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };

    function EnferByUseraved($usuario){
        $sql ="SELECT Enfermedad.Id_enfermedad,Enfermedad.Nombre_enfermedad,Enfermedad.video_url, Enfermedad.texto_url, Enfermedad.texto_url, 
        Enfermedad.Likes, Enfermedad.video_url2, Doctor.Id_doctor,usuario.Nombres, usuario.Email, usuario.Celular, usuario.Telefono, usuario.Foto,
        Enfermedad.texto_corto, Enfermedad.Nombre_video  FROM Enfermedad, Doctor,usuario,GuardadosAart WHERE Doctor.Id_doctor=Enfermedad.Id_doctor AND usuario.Email=Doctor.id_usuario 
        and GuardadosAart.Id_enfermedad=Enfermedad.Id_enfermedad and GuardadosAart.usuario='$usuario' ORDER BY Enfermedad.Valor asc,Enfermedad.Nombre_enfermedad"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };

    function guardarInfoArt($Nombres,$Telefono,$Email,$Pregunta,$Fecha_inicio,$Id_enfermedad,$Id_doc,$Revisado){
        $sql ="INSERT INTO Solicitarinfo (Nombres,Telefono,Email,Pregunta,Fecha_inicio,Id_enfermedad,Id_doc,Revisado) VALUES ('$Nombres','$Telefono','$Email','$Pregunta','$Fecha_inicio','$Id_enfermedad','$Id_doc','$Revisado')";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("info_ok");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };
    function updateLike($likes, $Id_enfermedad){
        $sql ="UPDATE Enfermedad SET Likes='$likes' WHERE Id_enfermedad='$Id_enfermedad' ";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("info_ok");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };

    function guardarLikesU($Id_enfermedad,$usuario){
        $sql ="INSERT INTO Likes_people (Id_enfermedad,Id_usuario) VALUES ('$Id_enfermedad','$usuario')";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("info_ok");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };

    function deleteLikesU($Id_enfermedad,$usuario){
        $sql ="DELETE FROM Likes_people WHERE Id_enfermedad='$Id_enfermedad' AND Id_usuario='$usuario'";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("info_ok");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };


    function EnferByid($Id_enfermedad){
        $sql ="SELECT Enfermedad.Id_enfermedad,Enfermedad.Nombre_enfermedad,Enfermedad.video_url, Enfermedad.texto_url, Enfermedad.texto_url, 
        Enfermedad.Likes, Enfermedad.video_url2, Doctor.Id_doctor,usuario.Nombres, usuario.Email, usuario.Celular, usuario.Telefono, usuario.Foto ,
        Enfermedad.texto_corto, Enfermedad.Nombre_video FROM Enfermedad, Doctor,usuario WHERE Doctor.Id_doctor=Enfermedad.Id_doctor AND usuario.Email=Doctor.id_usuario AND Enfermedad.Id_enfermedad='$Id_enfermedad'  LIMIT 30"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };

    function DocById($id_usuario){
        $sql ="SELECT * FROM Doctor WHERE id_usuario='$id_usuario' "; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            $contacto = $stmt->fetchObject();
            $db = null;
            echo  json_encode($contacto);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };
    function InstById($usuario){
        $sql ="SELECT * FROM Instituciones WHERE usuario='$usuario' "; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            $contacto = $stmt->fetchObject();
            $db = null;
            echo  json_encode($contacto);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };

    function EnfermEDICOSS($sql){
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };


    function GuardarFeed($Nombre_enfermedad,$Areas,$video_url,$video_url2,$texto_corto,$texto_url,$Sexo,$Edad_1, $Edad_2,$sintomas, $causas, $tratamiento,$diagnostico, $Nombre_video, $Id_doctor,$relacionadas){
        $sql ="INSERT INTO Enfermedad (Nombre_enfermedad, Areas, video_url, video_url2, texto_corto, texto_url, Sexo, Edad_1, Edad_2, sintomas, causas, tratamiento, diagnostico, Nombre_video, Id_doctor,Enfermedades_relacionadas) VALUES ('$Nombre_enfermedad','$Areas','$video_url','$video_url2','$texto_corto','$texto_url','$Sexo','$Edad_1', '$Edad_2', '$sintomas', '$causas', '$tratamiento','$diagnostico', '$Nombre_video',  '$Id_doctor','$relacionadas');";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("Feed correctamente");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };
    
    function EditFeed($Nombre_enfermedad,$Areas,$video_url,$video_url2,$texto_corto,$texto_url,$Sexo,$Edad_1, $Edad_2,$sintomas, $causas, $tratamiento,$diagnostico, $Nombre_video,$relacionadas,$Id_enfermedad){
        $sql ="UPDATE Enfermedad SET Nombre_enfermedad='$Nombre_enfermedad', Areas='$Areas', video_url='$video_url', video_url2='$video_url2', texto_corto='$texto_corto', texto_url='$texto_url', Sexo='$Sexo', Edad_1='$Edad_1', Edad_2='$Edad_2', sintomas='$sintomas', causas='$causas', tratamiento='$tratamientO', diagnostico='$diagnostico', Nombre_video='$Nombre_video', Enfermedades_relacionadas='$relacionadas' WHERE Id_enfermedad='$Id_enfermedad';";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("Feed correctamente");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };
    

    function addCasos($Sexo,$Edad_1, $Edad_2,$Descripcion,$Sintomas,$url_video,$Nombres_posibles, $causas, $resultados_exame, $doctores_visitados, $tratamientos,$id_doc){
        $sql ="INSERT INTO Casos_extraordinarios (Sexo, Edad_1, Edad_2, Descripcion, Sintomas, url_video, Nombres_posibles, causas, resultados_exame, doctores_visitados, tratamientos, id_doc) VALUES ('$Sexo','$Edad_1', '$Edad_2','$Descripcion','$Sintomas','$url_video','$Nombres_posibles','$causas', '$resultados_exame', '$doctores_visitados', '$tratamientos','$id_doc');";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("Casos  add correctamente");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };

    function UpdateCasos($Sexo,$Edad_1, $Edad_2,$Descripcion,$Sintomas,$url_video,$Nombres_posibles, $causas, $resultados_exame, $doctores_visitados, $tratamientos,$Id_casos){
        $sql ="UPDATE Casos_extraordinarios SET Sexo='$Sexo',Edad_1='$Edad_1',Edad_2='$Edad_2',Descripcion='$Descripcion',Sintomas='$Sintomas',url_video='$url_video',Nombres_posibles='$Nombres_posibles',causas='$causas',resultados_exame='$resultados_exame',doctores_visitados='$doctores_visitados',tratamientos='$tratamientos' WHERE Id_casos='$Id_casos'";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("Usuario correctamente");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };  

    function cuentaTotCasos(){
        $sql ="SELECT COUNT(Id_casos) as'Cuenta' FROM Casos_extraordinarios";
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            $contacto = $stmt->fetchObject();
            $db = null;
            echo  json_encode($contacto);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };
    
    function cuentaTotCasosDoc($id_doc){
        $sql ="SELECT COUNT(Id_casos) as'Cuenta' FROM Casos_extraordinarios where id_doc='$id_doc'";
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            $contacto = $stmt->fetchObject();
            $db = null;
            echo  json_encode($contacto);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };

    function CasosExep(){
        $sql ="SELECT * FROM Casos_extraordinarios  LIMIT 30"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };

    function CasosExepDoc($id_doc){
        $sql ="SELECT * FROM Casos_extraordinarios where id_doc='$id_doc' LIMIT 30"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };

    function ComentCasos($Id_caso){
        $sql ="SELECT * FROM Temp_Casos_coment where Id_caso='$Id_caso'"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };

    function addComent($Id_caso,$Id_usuario, $Comentario,$Nombres_coment,$Foto,$id_doc,$fecha_coment){
        $sql ="INSERT INTO Temp_Casos_coment (Id_caso,Id_usuario, Comentario,Nombres_coment,Foto,id_doc,fecha_coment) VALUES ('$Id_caso','$Id_usuario', '$Comentario','$Nombres_coment','$Foto','$id_doc','$fecha_coment');";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("Coment  add correctamente");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };

    function GiroNeg(){
        $sql ="SELECT * FROM Giro_negocio"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };

    function addProdServ($Nombre_comercial,$Nombre_generico, $Sexo,$Edad_1,$Edad_2,$Descripcion,$Id_institucion, $Giro_neg){
        $sql ="INSERT INTO Productos_Servicios (Nombre_comercial,Nombre_generico, Sexo,Edad_1,Edad_2,Descripcion,Id_institucion,Giro_neg) VALUES ('$Nombre_comercial','$Nombre_generico', '$Sexo','$Edad_1','$Edad_2','$Descripcion','$Id_institucion','$Giro_neg');";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("Prod  add correctamente");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };


    function addTemProdEspe($Id_prod_serv,$Id_especialidades){
        $sql ="INSERT INTO temp_espe_prod (Id_prod_serv,Id_especialidades) VALUES ('$Id_prod_serv','$Id_especialidades');";
        $db = getConnection();
        $stmt = $db->query($sql);  
        if ($stmt) {
            echo json_encode("Prod  add correctamente");
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($db);
        }
    };

    function SelecTemProdEspe($Id_prod_serv){
        $sql =" SELECT Especialidades.Nombre_especialidad, Especialidades.Id_especialidad FROM Especialidades,Productos_Servicios ,temp_espe_prod WHERE Productos_Servicios.Id_prod_serv=temp_espe_prod.Id_prod_serv AND Especialidades.Id_especialidad=temp_espe_prod.Id_especialidades AND temp_espe_prod.Id_prod_serv='$Id_prod_serv'"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };
    function SelectProdByInsti($Id_institucion){
        $sql =" SELECT * FROM Productos_Servicios WHERE Id_institucion='$Id_institucion'"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };
    function SelectProdByGiro($Giro_neg){
        $sql =" SELECT * FROM Productos_Servicios WHERE Giro_neg='$Giro_neg'"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            while($row  = $stmt->fetch(PDO::FETCH_OBJ))
            {
                $data[] = $row;
            }
            if(!isset($data)){
                $data="no_register";
            }
            echo json_encode($data);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };
    function ultimoProd(){
        $sql ="SELECT * FROM Productos_Servicios order by Id_prod_serv desc limit 1"; 
        try {
            $db = getConnection();
            $stmt = $db->query($sql);  
            $contacto = $stmt->fetchObject();
            $db = null;
            echo  json_encode($contacto);
        } catch(PDOException $e) {
            echo '{"error":{"text":'. $e->getMessage() .'}}'; 
        }
    };