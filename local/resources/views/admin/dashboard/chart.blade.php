<div class="row">
<div class="col-lg-6  m-b-30">
<div class="card">
<div class="card-header">
<div class="card-title">Reporte Pedidos ultimos 6 meses</div>

<div class="card-controls">

<a href="#" class="js-card-refresh icon"> </a>

</div>

</div>
<div class="card-body">
<div id="chart-01"></div>
</div>
<div class="">
</div>
<div class="card-footer">
<div class="d-flex  justify-content-between">
<h6 class="m-b-0 my-auto"><span class="opacity-75"> <i class="mdi mdi-information"></i> Deseo ver mas info</span>
</h6>
</div>
</div>
</div>
</div>
<div class="col-lg-6  m-b-30">
<div class="card">
<div class="card-header">
<div class="card-title">Top Tienda con mas Pedidos</div>

<div class="card-controls">

<a href="#" class="js-card-refresh icon"> </a>

</div>

</div>
<div class="card-body">


<div id="chart-02"></div>
</div>
<div class="">
</div>
<div class="card-footer">
<div class="d-flex  justify-content-between">
<h6 class="m-b-0 my-auto"><span class="opacity-75"> <i class="mdi mdi-information"></i>Deseo ver mas info</span>
</h6>
</div>
</div>
</div>
</div>
</div>
