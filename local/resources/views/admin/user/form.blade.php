<div class="card py-3 m-b-30">
    <div class="card-body">
        @include('admin.language.header')
    </div>
</div>

<div class="tab-content" id="myTabContent1">

    @foreach(DB::table('language')->orderBy('sort_no','ASC')->get() as $l)

    <div class="tab-pane fade show" id="lang{{ $l->id }}" role="tabpanel" aria-labelledby="lang{{ $l->id }}-tab">

        <input type="hidden" name="lid[]" value="{{ $l->id }}">

        <div class="card py-3 m-b-30">
            <div class="card-body">

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="asd">Nombre</label>
                        {!! Form::text('l_name[]',$data->getSData($data->s_data,$l->id,0),['placeholder' =>
                        'Name','class' => 'form-control'])!!}
                    </div>

                    <div class="form-group col-md-6">
                        <label for="asd">Dirección</label>
                        {!! Form::text('l_address[]',$data->getSData($data->s_data,$l->id,1),['placeholder' => 'Store
                        Address','class' => 'form-control'])!!}
                    </div>
                </div>

            </div>
        </div>

    </div>
    @endforeach

    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

        <div class="card py-3 m-b-30">
            <div class="card-body">

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail6">Nombre Comercial</label>
                        {!! Form::text('name',null,['required' => 'required','placeholder' => 'Name','class' =>
                        'form-control'])!!}
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Email </label>
                        {!! Form::email('email',null,['required' => 'required','placeholder' => 'Email Address','class'
                        => 'form-control'])!!}
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="inputEmail6">Telefono</label>
                        {!! Form::text('phone',null,['required' => 'required','placeholder' => 'Contact Number','class'
                        => 'form-control'])!!}
                    </div>
{{ $data }}

                    <div class="form-group col-md-3">
                        <label for="inputEmail4">Tipo Tienda</label>
                        <select name="store_type" class="form-control" required="required">
                            <option value="">Selección</option>
                            <option value="Restaurants" onclick=" {{ $store_type = 'Restaurants' }}">Restaurantes</option>
                            <option value="Productos">Productos</option>
                            <option value="Oficios">Oficios</option>
                            <option value="Encargos">Encargos - Logística</option>
                            <option value="Restaurants">Emprendimientos</option>
                        </select>
                    </div>
                    
                    
                    @if($data->store_type == "Restaurants")
                    <div class="form-group col-md-3">
                        <label for="inputEmail4">Restaurantes</label>
                        <select name="store_type_second" class="form-control" required="required">
                            <option value="">Selección</option>
                            <option value="Asadero">Asadero</option>
                            <option value="Bar">Bar</option>
                            <option value="Bistro">Bistro</option>
                            <option value="Burguer">Burguer</option>
                            <option value="Cafeteria">Cafeteria</option>
                            <option value="Chifa">Chifa</option>
                            <option value="Comida mexicana">Comida mexicana</option>
                            <option value="Comida Tipica">Comida Tipica</option>
                            <option value="Heladeria">Heladeria</option>
                            <option value="Marisqueria">Marisqueria</option>
                            <option value="Panaderia">Panaderia</option>
                            <option value="Pasteleria">Pasteleria</option>
                            <option value="Pincheria">Pincheria</option>
                            <option value="Pizzeria">Pizzeria</option>
                            <option value="Polleria">Polleria</option>
                            <option value="Pub Bar">Pub Bar</option>
                            <option value="Reposteria">Reposteria</option>
                            <option value="Restaurant">Restaurant</option>
                            <option value="Sanduches">Sanduches</option>
                            <option value="Sushi">Sushi</option>
                            <option value="Tratoria">Tratoria</option>
                        </select>
                    </div>
                    @endif

                    <div class="form-group col-md-3">
                        <label for="inputEmail4">Ciudad</label>
                        <select name="city_id" class="form-control" required="required">
                            <option value="">Selección Ciudad</option>
                            @foreach($citys as $city)
                            <option value="{{ $city->id }}" @if($data->city_id == $city->id) selected
                                @endif>{{ $city->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail6">Dirección</label>
                        {!! Form::text('address',null,['required' => 'required','placeholder' => 'Full Address','class'
                        => 'form-control'])!!}
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Logo de la empresa (recommended size 600 * 400)</label>
                        <input type="file" name="img" class="form-control" @if(!$data->id) required="required" @endif>
                    </div>
                </div>

                @if(isset($type))

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="inputEmail6">Cambiar Contraseña </label>
                        <input type="Password" name="password" class="form-control">
                    </div>
                </div>

                @else

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail6">Contraseña</label>
                        <input type="Password" name="password" class="form-control" @if(!$data->id) required="required"
                        @endif>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Estado</label>
                        <select name="status" class="form-control">
                            <option value="0" @if($data->status == 0) selected @endif>Activo</option>
                            <option value="1" @if($data->status == 1) selected @endif>Inactivo</option>
                        </select>
                    </div>
                </div>
                @endif

                @if($data->img)

                <img src="{{ Asset('upload/user/'.$data->img) }}" width="50px"><br><br>

                @endif

            </div>
        </div>

        <h1 style="font-size: 20px">Información Propietario</h1>
        <div class="card py-3 m-b-30">
            <div class="card-body">

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail6">Ruc - Cédula </label>
                        {!! Form::number('ruc',null,['required' => 'required','placeholder' => 'Ruc o cedula','class' =>
                        'form-control'])!!}
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Representante legal / Nombre Porpietario </label>
                        {!! Form::text('name_prop',null,['required' => 'required','placeholder' => 'Nombres del
                        propietario','class'
                        => 'form-control'])!!}
                    </div>
                </div>

            </div>
        </div>


        @if(isset($admin))

        <input type="hidden" name="admin" value="1">

        <h1 style="font-size: 20px">Establecer cargos de comisión</h1>
        <div class="card py-3 m-b-30">
            <div class="card-body">

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail6">Tipo de comisión</label>
                        <select name="c_type" class="form-control">
                            <option value="0" @if($data->c_type == 0) selected @endif>Valor fijo</option>
                            <option value="1" @if($data->c_type == 1) selected @endif>Pedido %</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="inputEmail6">Valor de la comisión</label>
                        {!! Form::text('c_value',null,['class' => 'form-control'])!!}
                    </div>
                </div>

            </div>
        </div>
        @endif

        <h1 style="font-size: 20px">Gastos de envio y horarios</h1>
        <div class="card py-3 m-b-30">
            <div class="card-body">

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail6">Valor mínimo del carrito</label>
                        {!! Form::text('min_cart_value',null,['placeholder' => 'Después de esta cantidad, la entrega
                        será gratuita','class' => 'form-control'])!!}
                    </div>

                    <div class="form-group col-md-6">
                        <label for="inputEmail6">Valor envio</label>
                        {!! Form::number('delivery_charges_value',null,['class' => 'form-control'])!!}
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail6">Hora de apertura <i>(Seleccione 00 si siempre está abierta)</i></label>
                        <select name="opening_time" class="form-control">
                            <option>Selección</option>
                            @php($ot = 0)

                            @while($ot < 23) @php($ot++) <option value="{{ $ot.":00" }}" @if($data->opening_time == $ot)
                                selected @endif>{{ $ot.":00" }}</option>
                                <option value="{{ $ot.":30" }}" @if($data->opening_time == $ot.':30') selected @endif>
                                    {{ $ot }}:30</option>

                                @endwhile
                                <option value="00" @if($data->opening_time == '00') selected @endif>00</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="inputEmail6">Hora de cierre <i>(select 23:30 if always open)</i></label>
                        <select name="closing_time" class="form-control">
                            <option>Selección</option>
                            @php($ct = 0)

                            @while($ct < 23) @php($ct++) <option value="{{ $ct.":00" }}" @if($data->closing_time == $ct)
                                selected @endif>{{ $ct.":00" }}</option>
                                <option value="{{ $ct.":30" }}" @if($data->closing_time == $ct.":30") selected
                                    @endif>{{ $ct }}:30</option>

                                @endwhile
                                <option value="00" @if($data->closing_time == '00') selected @endif>00</option>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail6">Tiempo de entrega estimado <small>(minutos)</small></label>
                        {!! Form::text('delivery_time',null,['placeholder' => 'e.g 20-25','class' => 'form-control'])!!}
                    </div>

                    <div class="form-group col-md-6">
                        <label for="inputEmail6">Costo aproximado por persona <small>(no incluya ningún signo de
                                moneda)</small></label>
                        {!! Form::text('person_cost',null,['placeholder' => 'e.g 200-250','class' => 'form-control'])!!}
                    </div>
                </div>
            </div>
        </div>

        <h1 style="font-size: 20px">Imágenes Adicionales</h1>
        <div class="card py-3 m-b-30">
            <div class="card-body">

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="inputEmail4">Seleccionar imágenes (para selección múltiple con CTRl)</label>
                        <input type="file" name="gallery[]" class="form-control" multiple="true">
                    </div>
                </div>

                @if(isset($images))
                <div class="form-row">
                    @foreach($images as $img)
                    <div class="form-group col-md-2">
                        <img src="{{ Asset('upload/user/gallery/'.$img->img) }}" width="50%"><br>
                        <a href="{{ Asset(env('admin').'/imageRemove/'.$img->id) }}"
                            onclick="return confirm('Are you sure?')" style="color:Red">Eliminar</a>
                    </div>
                    @endforeach
                </div>
                @endif

            </div>
        </div>
        <h3 style="font-size: 20px;">Seleccionar ubicación del mapa de Google <small>(Para buscar según la ubicación en
                la aplicación)</small></h3>
        <div class="card py-3 m-b-30">
            <div class="card-body">

                @include('admin.user.google')

            </div>
        </div>
    </div>
</div>
<button type="submit" class="btn btn-success btn-cta">Guardar</button><br><br>