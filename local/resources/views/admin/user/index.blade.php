@extends('admin.layout.main')

@section('title') Administrar Tiendas @endsection

@section('icon') mdi-home @endsection


@section('content')

<section class="pull-up">
<div class="container">
<div class="row ">
<div class="col-md-12">
<div class="card py-3 m-b-30">

<div class="row">
<div class="col-md-12" style="text-align: right;"><a href="{{ Asset($link.'add') }}" class="btn m-b-15 ml-2 mr-2 btn-rounded btn-warning">Añadir</a>&nbsp;&nbsp;&nbsp;</div>

</div>


<div class="card-body">
<table class="table table-hover ">
<thead>
<tr>
<th>Imagen</th>
<th>Nombre</th>
<th>Teléfono</th>
<th>Ciudad</th>
<th>Estado</th>
<th style="text-align: right">Opciones</th>
</tr>

</thead>
<tbody>

@foreach($data as $row)

<tr>
<td width="10%"><img src="{{ Asset('upload/user/'.$row->img) }}" height="40"></td>
<td width="15%">{{ $row->name }}<br><small>{{ $row->type }}</small></td>
<td width="12%">{{ $row->phone }}</td>
<td width="12%">{{ $row->city }}</td>
<td width="12%">

@if($row->status == 0)

<button type="button" class="btn btn-sm m-b-15 ml-2 mr-2 btn-info" onclick="confirmAlert('{{ Asset($link.'status/'.$row->id) }}')">Activo</button>

@else

<button type="button" class="btn btn-sm m-b-15 ml-2 mr-2 btn-danger" onclick="confirmAlert('{{ Asset($link.'status/'.$row->id) }}')">Inactivo</button>

@endif

</td>

<td width="35%" style="text-align: right">


<a href="javascript::void()" class="btn m-b-15 ml-2 mr-2 btn-md  btn-rounded-circle <?php if($row->open == 1){ echo "btn-danger"; } else { echo "btn-success"; } ?>" data-toggle="tooltip" data-placement="top" data-original-title="<?php if($row->open == 1){ echo "Cerrado ahora"; } else { echo "Abierto Ahora"; } ?>" onclick="confirmAlert('{{ Asset($link.'status/'.$row->id.'?type=open') }}')"><i class="mdi mdi-disc"></i></a>

<a href="javascript::void()" class="btn m-b-15 ml-2 mr-2 btn-md  btn-rounded-circle <?php if($row->trending == 1){ echo "btn-success"; } else { echo "btn-warning"; } ?>" data-toggle="tooltip" data-placement="top" data-original-title="<?php if($row->trending == 1){ echo "en Marketing"; } else { echo "Publicar con Marketing Inicial"; } ?>" onclick="confirmAlert('{{ Asset($link.'status/'.$row->id.'?type=trend') }}')"><i class="mdi mdi-trending-up"></i></a>


<a href="{{ Asset(env('admin').'/loginWithID/'.$row->id) }}" class="btn m-b-15 ml-2 mr-2 btn-md  btn-rounded-circle btn-info" data-toggle="tooltip" data-placement="top" data-original-title="Iniciar sesión como usuario" target="_blank"><i class="mdi mdi-login"></i></a>

<a href="javascript::void()" class="btn m-b-15 ml-2 mr-2 btn-md  btn-rounded-circle btn-primary" data-toggle="tooltip" data-placement="top" data-original-title="Ver detalles de inicio de sesión" onclick="showMsg('Username : {{ $row->email }}<br>Contraseña : {{ $row->shw_password }}')"><i class="mdi mdi-lock"></i></a>

<a href="{{ Asset($link.$row->id.'/edit') }}" class="btn m-b-15 ml-2 mr-2 btn-md  btn-rounded-circle btn-success" data-toggle="tooltip" data-placement="top" data-original-title="Editar"><i class="mdi mdi-border-color"></i></a>

<button type="button" class="btn m-b-15 ml-2 mr-2 btn-md  btn-rounded-circle btn-danger" data-toggle="tooltip" data-placement="top" data-original-title="Borrar" onclick="deleteConfirm('{{ Asset($link."delete/".$row->id) }}')"><i class="mdi mdi-delete-forever"></i></button>


</td>
</tr>

@endforeach

</tbody>
</table>

</div>
</div>
</div>
</div>
</div>
</section>

@endsection