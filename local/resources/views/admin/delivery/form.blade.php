
<div class="form-row">

<div class="form-group col-md-12">
<label for="inputEmail6">Nombre</label>
{!! Form::text('name',null,['id' => 'code','class' => 'form-control','required' => 'required'])!!}
</div>
</div>

<div class="form-row">
<div class="form-group col-md-6">
<label for="inputEmail6">Telefono (Puede ser el username)</label>
{!! Form::text('phone',null,['id' => 'code','class' => 'form-control','required' => 'required'])!!}
</div>

<div class="form-group col-md-6">
@if($data->id)
<label for="inputEmail6">Cambiar Contraseña</label>
<input type="password" name="password" class="form-control">
@else
<label for="inputEmail6">Contraseña</label>
<input type="password" name="password" class="form-control" required="required">
@endif
</div>
</div>

<div class="form-row">
<div class="form-group col-md-12">
<label for="inputEmail6">Estado</label>
<select name="status" class="form-control">
	<option value="0" @if($data->status == 0) selected @endif>Activo</option>
	<option value="1" @if($data->status == 1) selected @endif>Inactivo</option>
</select>
</div>
</div>


<button type="submit" class="btn btn-success btn-cta">Guardar</button>
