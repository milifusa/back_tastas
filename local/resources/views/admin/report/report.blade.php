<p align="center" style="font-size: 20px">Reporte de  {{ $from }} a {{ $to }}</p>

<table width="100%" cellspacing="0" cellpadding="0" border="1">

<tr>
<td>&nbsp;<b>Pedido ID</b></td>
<td>&nbsp;<b>Fecha</b></td>
<td>&nbsp;<b>Usuario</b></td>
<td>&nbsp;<b>Tienda</b></td>
<td>&nbsp;<b>Monto Total</b></td>
<td>&nbsp;<b>Comisión</b></td>
</tr>

@php($total = [])
@php($com = [])
@foreach($data as $row)
@php($total[] = $row['amount'])
@php($com[] = $user->getCom($row['id'],$row['amount']))
<tr>
<td width="17%">&nbsp;#{{ $row['id'] }}</td>
<td width="17%">&nbsp;{{ $row['date'] }}</td>
<td width="17%">&nbsp;{{ $row['user'] }}</td>
<td width="17%">&nbsp;{{ $row['store'] }}</td>
<td width="17%">&nbsp;{{ $row['amount'] }}</td>
<td width="15%">&nbsp;{{ $user->getCom($row['id'],$row['amount']) }}</td>
</tr>

@endforeach	

<tr>
<td width="17%">&nbsp;</td>
<td width="17%">&nbsp;<b>Total Orders</b></td>
<td width="17%">&nbsp;<b>{{ count($total) }}</b></td>
<td width="17%">&nbsp;<b>Total</b></td>
<td width="17%">&nbsp;<b>{{ array_sum($total) }}</b></td>
<td width="15%">&nbsp;<b>{{ array_sum($com) }}</b></td>
</tr>

</table>