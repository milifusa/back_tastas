@extends('user.layout.main')

@section('title') Imprimir Recibo @endsection

@section('icon') mdi-file @endsection


@section('content')

<div class="pull-up">
<div class="container" id="printableArea">
<div class="row"  >
<div class="col-md-12 m-b-40">
<div class="card">
<div class="card-body">
<div class="row">
<div class="col-md-6">

<address class="m-t-10">
To,<br>
<span class="h4 font-primary"> {{ $order->name }}</span> <br>
{{ $order->phone }}<br>
{{ $order->email }}<br>
{{ $order->address }}<br>
{{ $order->city }}<br>


</address>
</div>
<div class="col-md-6 text-right my-auto">
<h1 class="font-primary">Recibo</h1>
<div class="">Pedido ID: #{{ $order->id }}</div>
<div class="">Fecha: {{ date('d-M-Y',strtotime($order->created_at)) }}</div>
</div>
</div>

<div class="table-responsive ">
<table class="table m-t-50">
<thead>
<tr>
<th width="40%">Nombre Item</th>
<th class="text-center">Precio</th>
<th class="text-center">Cantidad</th>
<th class="text-right">Total</th>
</tr>
</thead>
<tbody>
@php($total = [])
@foreach($items as $item)
@php($total[] = $item['qty'] * $item['price'])

<tr>
<td width="40%">{{ $item['type'] }} - {{ $item['item'] }}</td>
<td width="20%" class="text-center">{{ $item['price'] }}</td>
<td width="20%" class="text-center">{{ $item['qty'] }}</td>
<td width="20%" class="text-right">{{ $currency.$item['qty'] * $item['price'] }}</td>
</tr>

@foreach($it->getAddon($item['id'],$order->id) as $add)

<tr>
<td width="40%">{{ $add->addon }}</td>
<td width="20%" class="text-center">{{ $currency.$add->price }}</td>
<td width="20%" class="text-center">{{ $add->qty  }}</td>
<td width="20%" class="text-right">{{ $currency.$add->qty * $add->price }}</td>
</tr>

@endforeach

@endforeach

<tr>
<td width="40%">&nbsp;</td>
<td width="20%">&nbsp;</td>
<td width="20%" class="text-center"><b>Total</b></td>
<td width="20%" class="text-right"><b>{{ $currency.array_sum($total) }}</b></td>
</tr>

@if($order->discount)

<tr>
<td width="40%">&nbsp;</td>
<td width="20%">&nbsp;</td>
<td width="20%" class="text-center"><b>Descuento</b></td>
<td width="20%" class="text-right"><b>{{ $currency.$order->discount }}</b></td>
</tr>

@endif


@if($order->d_charges)

<tr>
<td width="40%">&nbsp;</td>
<td width="20%">&nbsp;</td>
<td width="20%" class="text-center"><b>Costo envio</b></td>
<td width="20%" class="text-right"><b>{{ $currency.$order->d_charges }}</b></td>
</tr>

@endif

<tr>
<td width="40%">&nbsp;</td>
<td width="20%">&nbsp;</td>
<td width="20%" class="text-center"><b>Sub Total</b></td>
<td width="20%" class="text-right"><b>{{ $currency.$order->total }}</b></td>
</tr>

</tbody>
</table>
</div>
<div class="p-t-10 p-b-20">
<p class="text-muted ">

@if($order->payment_method == 1)

<b>Payment Method : </b>pagar en efectivo cuando se realiza la entrega<br><br>

@else

<b>Payment Method : </b> pagar via PayPal<br><br>


@endif

Los servicios se facturarán de acuerdo con la Descripción del servicio.

<!-- Services will be invoiced in accordance with the Service Description. You must
pay all undisputed invoices in full within 30 days of the invoice date, unless
otherwise specified under the Special Terms and Conditions. All payments must
reference the invoice number. Unless otherwise specified, all invoices shall be
paid in the currency of the invoice -->
</p>
<hr>
<div class="text-center opacity-75">
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

@endsection